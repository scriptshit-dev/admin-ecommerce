import React, { useState } from 'react';
import { Route, Redirect } from 'react-router';
import { IonLoading, IonApp, IonSplitPane, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import {speedometer,
        pricetags,
        people,
        card,
        radioButtonOn,
        podium,
        rose
} from 'ionicons/icons';
import { ProvideAuth, useAuth } from './authentication';

import { AppPage } from './Admin/declarations';
import Menu from './Admin/components/Menu';
import User from './Admin/pages/User';
import Login from './Login';
import EditProduct from './Admin/pages/EditProduct';
import Dashboard from './Admin/pages/Dashboard';
import Product from './Admin/pages/Product';
import Validate from './Admin/pages/Validate';
import Orders from './Admin/pages/Orders';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

const App: React.FC = () => {
  return (
    <ProvideAuth>
      <Admin />
    </ProvideAuth>
  );
}

const appPages: AppPage[] = [
  {
    title: 'Dashboard',
    url: '/Dashboard',
    icon: speedometer
  },
  {
    title: 'Katalog',
    url: '/Dashboard',
    icon: pricetags
  },
  {
    title: 'Produk',
    url: '/Product',
    icon: rose
  },
  {
    title: 'Pengguna',
    url: '/User',
    icon: people
  },
  {
    title: 'Transaksi',
    url: '/Dashboard',
    icon: card
  },
  {
    title: 'Penjualan',
    url: '/Orders',
    icon: podium
  },
  {
    title: 'Validasi Pembayaran',
    url: '/Validate',
    icon: radioButtonOn
  },
];

const Admin: React.FC = () => {
  const auth = useAuth();
  const [loading, setLoading] = useState(true)

  if (auth.state.initializing) {
    return <IonLoading
      isOpen={loading}
      onDidDismiss={() => setLoading(false)}
      message={'Loading...'}
      duration={5000}
    />
  }

  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          {!auth.state.user ? '' : <Menu appPages={appPages} />}
          <IonRouterOutlet id="main">
            <Route path="/Dashboard" component={Dashboard} exact={true} />
            <Route path="/Product" component={Product} exact={true} />
            <Route path="/User" component={User} exact={true} />
            <Route path="/Login" component={Login} exact={true} />
            <Route path="/Validate" component={Validate} exact={true} />
            <Route path="/Orders" component={Orders} exact={true} />
            <Route path="/EditProduct/:id" component={EditProduct} exact={true} />
            {!auth.state.user ? 
            <Route path="/" render={() => <Redirect to="/Login" />} exact={true} /> :
            <Route path="/" render={() => <Redirect to="/Dashboard" />} exact={true} />
            }
          </IonRouterOutlet>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  )
};

export default App;