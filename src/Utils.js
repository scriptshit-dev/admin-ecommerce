import React from "react";
import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";

let db = firebase.firestore();
let storage = firebase.storage();

class Utils extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: [],
      detailUser: [],
      transaction: [],
      pending: [],
      detailTransaksi: [],
      list: [],
      details: [],
      error: "",
    };
  }

  strg() {
    return storage;
  }

  async addProduct(a, b, c, d, e, f, g, h, i, j) {
    await db
      .collection("produk")
      .add({
        nama: a,
        harga: b,
        kategori: c,
        jenis: d,
        penjual: e,
        desc: f,
        foto: g,
        stok: h,
        noHp: i,
        norek: j,
      })
      .then(function (docRef) {
        console.log("Document Witten ID: ", docRef.id);
      })
      .catch(function (error) {
        console.error("Error Adding Document ", error);
      });
  }

  async getUser() {
    await db
      .collection("users")
      .get()
      .then((snap) => {
        this.state.user = [];
        snap.forEach((doc) => {
          this.state.user.push({
            id: doc.id,
            ...doc.data(),
          });
        });
      });
  }

  async getDetailUser(id) {
    await db
      .collection("users")
      .doc(id)
      .get()
      .then((snap) => {
        this.state.detailUser = [];
        this.state.detailUser.push({
          id: snap.id,
          ...snap.data(),
        });
      });
  }

  async getTransactionUsers() {
    await db
      .collection("users")
      .get()
      .then((snap) => {
        this.state.transaction = [];
        this.state.pending = [];
        snap.forEach((doc) => {
          db.collection("users")
            .doc(doc.id)
            .collection("transaksi")
            .get()
            .then((snap2) => {
              snap2.forEach((doc2) => {
                if (
                  doc2.data().status === "Pending" ||
                  doc2.data().status === "Process"
                ) {
                  this.state.pending.push({
                    idUser: doc.id,
                    idTransaksi: doc2.id,
                    ...doc2.data(),
                  });
                } else {
                  this.state.transaction.push({
                    idUser: doc.id,
                    idTransaksi: doc2.id,
                    ...doc2.data(),
                  });
                }
              });
            });
        });
      });
  }

  async getDetailTransactionUser(idUser, idTransaksi) {
    await db
      .collection("users")
      .doc(idUser)
      .collection("transaksi")
      .doc(idTransaksi)
      .get()
      .then((doc) => {
        this.state.detailTransaksi = [];
        this.state.detailTransaksi.push({
          idTransaksi: doc.id,
          ...doc.data(),
        });
      });
  }

  async processTransaction(idUser, idTransaksi, status) {
    await db
      .collection("users")
      .doc(idUser)
      .collection("transaksi")
      .doc(idTransaksi)
      .update({ status: status });
  }

  async deleteTransactionUser(idUser, idTransaksi) {
    try {
      await db
        .collection("users")
        .doc(idUser)
        .collection("transaksi")
        .doc(idTransaksi)
        .delete();
    } catch (e) {
      console.error(e);
    }
  }

  async updateStock(id, jml) {
    await db
      .collection("produk")
      .doc(id)
      .update({ stok: firebase.firestore.FieldValue.increment(+jml) });
  }

  async getBarang() {
    await db
      .collection("produk")
      .get()
      .then((snap) => {
        snap.forEach((doc) => {
          this.state.list.push({
            id: doc.id,
            ...doc.data(),
          });
        });
      });
  }

  async getDetails(id) {
    await db
      .collection("produk")
      .doc(id)
      .get()
      .then((doc) => {
        this.state.details.push({
          id: doc.id,
          ...doc.data(),
        });
      });
  }

  async deleteProduk(id, foto) {
    try {
      await storage.refFromURL(foto).delete();
      await db.collection("produk").doc(id).delete();
    } catch (e) {
      console.error(e);
    }
  }

  async editProduct(id, a, b, c, d, e, f, g, h, i) {
    await db
      .collection("produk")
      .doc(id)
      .update({
        nama: a,
        harga: b,
        kategori: c,
        jenis: d,
        penjual: e,
        desc: f,
        stok: g,
        noHp: h,
        norek: i,
      })
      .then(function (docRef) {
        console.log(docRef, " Berhasil Diupdate!");
      })
      .catch(function (error) {
        console.log(error, " gagal");
      });
  }

  async editFotoProduk(id, a) {
    await db.collection("produk").doc(id).update({ foto: a });
  }

  getB() {
    this.getBarang();
    console.log(this.state.list);
  }
}

export default new Utils();
