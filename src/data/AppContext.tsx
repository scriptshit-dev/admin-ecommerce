import { createContext } from 'react';

interface AppContextState {
  user?: firebase.User;
  admin?: boolean;
  isLoggedin?: boolean
}

export const AppContext = createContext<AppContextState>({
  user: undefined,
  admin: false,
  isLoggedin: false
});