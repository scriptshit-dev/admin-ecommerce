import firebase from 'firebase';
import 'firebase/auth';
import { useContext } from "react";
import { AppContext } from "./AppContext";

export const useSession = () => {
  const { user } = useContext(AppContext);
  return user;
}

export const loginWithEmail = async (email: string, password: string) => {
  try {
    const results = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password);
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const createUserWithEmail = async (email: string, password: string) => {
  try {
    await firebase
      .auth()
      .createUserWithEmailAndPassword(email, password);
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const signOut = () => firebase.auth().signOut();