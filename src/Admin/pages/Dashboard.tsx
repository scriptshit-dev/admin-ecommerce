import React, { useState, useEffect } from 'react';
import {
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton,
  IonIcon,
  IonText,
  IonRefresher,
  IonSpinner,
  IonItemDivider
} from '@ionic/react';
import { useHistory } from 'react-router';
import { RefresherEventDetail } from '@ionic/core';
import { card, cart, people } from 'ionicons/icons';
import Utils from '../../Utils';
import './Dashboard.css';

const DashboardPage: React.FC = () => {
  let history = useHistory();
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState<any[]>([]);
  const [transaksi, setTransaksi] = useState<any[]>([]);
  const [state, setState] = useState(0);
  const [pending, setPending] = useState([]);


  useEffect(() => {
    const showAll = async()=> {
      setLoading(true);
      try {
        await Utils.getTransactionUsers();
        await Utils.getUser();
        // await Utils.getPending();
        
        setTimeout(() => {
          setTransaksi(transaksi.splice(0,100));
          setUser(user.splice(0,100));
          setPending(pending.splice(0,100));
        }, 1000);

        setTimeout(() => {
          setPending(Utils.state.pending);
          setTransaksi(Utils.state.transaction);
          setUser(Utils.state.user);
          setLoading(false);
        }, 2000);
      } catch (err) {
        console.error(err);
      }
    }
    showAll();
  }, [state]);

  const doRefresh = (event: CustomEvent<RefresherEventDetail>) => {
    setState(state + 1);
    setTimeout(() => {
        event.detail.complete();
    }, 1000);
  }

  function page(a:any){
    switch(a){
      case 'Validate':
        history.replace("/"+a);
        break;
      case 'User':
        history.replace("/"+a);
        break;
      case 'Orders':
        history.replace("/"+a);
        break;
    }
  }

  return (
    <IonPage className="dashboard-page">
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Dashboard</IonTitle>
        </IonToolbar>
      </IonHeader>
      <div className="dashboard"><h3>Dashboard</h3> Home > Dashboard</div>
      <IonItemDivider/>
      <IonContent>
      <IonRefresher slot="fixed" onIonRefresh={doRefresh} />
        {loading ? <IonSpinner name="circles"/> : 
        <div>
        <IonCard className="welcome-card" >
        <IonCardHeader className="card-header">
          <IonCardTitle className="content-header">PENJUALAN</IonCardTitle>
        </IonCardHeader>
        <IonCardContent className="content-center">
          <div>
            <IonIcon icon={cart} className="ionicon" />
            <IonText className="iontext">{transaksi.length}</IonText>
          </div>
        </IonCardContent>
        <IonButton className="content-bottom" expand="full" onClick={()=>page('Orders')}>View more...</IonButton>
      </IonCard>

      <IonCard className="welcome-card" >
        <IonCardHeader className="card-header">
          <IonCardTitle className="content-header">VALIDASI PEMBAYARAN</IonCardTitle>
        </IonCardHeader>
        <IonCardContent className="content-center">
          <div>
            <IonIcon icon={card} className="ionicon" />
            <IonText className="iontext">{pending.length}</IonText> 
          </div>
        </IonCardContent>
        <IonButton className="content-bottom" expand="full" onClick={()=>page('Validate')}>View more...</IonButton>
      </IonCard>

      <IonCard className="welcome-card" >
        <IonCardHeader className="card-header">
          <IonCardTitle className="content-header">PENGGUNA</IonCardTitle>
        </IonCardHeader>
        <IonCardContent className="content-center">
          <div>
            <IonIcon icon={people} className="ionicon" />
            <IonText className="iontext">{user.length}</IonText>
          </div>
        </IonCardContent>
        <IonButton className="content-bottom" expand="full" onClick={()=>page('User')}>View more...</IonButton>
      </IonCard> </div>}

      </IonContent>
    </IonPage>
  );
};

export default DashboardPage;