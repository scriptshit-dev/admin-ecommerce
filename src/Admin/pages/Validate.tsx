import {
    IonButtons,
    IonCard,
    IonCardContent,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonText,
    IonGrid,
    IonRow,
    IonSpinner,
    IonCol,
    IonButton,
    IonModal,
    IonIcon,
    IonLabel,
    IonItemDivider,
    IonRefresher,
    IonAlert,
    IonCardHeader,
    IonCardTitle,
    IonPopover,
} from '@ionic/react';
import { RefresherEventDetail } from '@ionic/core';
import { close } from 'ionicons/icons';
import React, { useState, useEffect } from 'react';
import './Dashboard.css';
import Utils from '../../Utils';

interface update {
    idUser?: string,
    idTransaksi: string,
    status: string,
}

const defaultUpdate = { idUser: '', idTransaksi: '', status: '' };

const ValidatePage: React.FC = () => {
    const [loading, setLoading] = useState(false);
    const [popOver, setPopOver] = useState(false);
    const [popOverImage, setPopOverImage] = useState(false);
    const [alert, showAlert] = useState(false);

    const [pending, setPending] = useState<any[]>([]);
    const [state, setState] = useState(0);
    const [modal, setModal] = useState(false);
    const [detailTransaksi, setDetailTransaksi] = useState<any[]>([]);
    const [detailUser, setDetailUser] = useState<any[]>([]);
    const [message, setMessage] = useState();
    const [buttonAlert, setButtonAlert] = useState("");
    const [update, setUpdate] = useState<update>({
        idUser: '',
        idTransaksi: '',
        status: ''
    });

    useEffect(() => {
        async function showPendingTransaction() {
            setLoading(true);
            try {
                await Utils.getTransactionUsers()
                    .then(() => {

                        setTimeout(() => {
                            setPending(pending.splice(0, 100));
                        }, 1000);

                        setTimeout(() => {
                            setPending(Utils.state.pending);
                            setLoading(false);
                        }, 1500);
                    })

            } catch (err) {
                console.error(err);
            }
        }
        showPendingTransaction();
    }, [state]);

    const openModal = (a: any, b: any) => {
        setModal(true);
        const openDetailTransaction = async () => {
            detailTransaksi.splice(0, 100);
            try {
                await Utils.getDetailTransactionUser(a, b)
                    .then(() => {
                        setDetailTransaksi(Utils.state.detailTransaksi);
                        setUpdate({ idUser: a, idTransaksi: b, status: '' });
                    })
                    .catch((e) => {
                        console.error(e);
                    })
            } catch (e) { }
        }

        const openDetailUser = async () => {
            detailUser.splice(0, 100);
            try {
                await Utils.getDetailUser(a)
                    .then(() => {
                        setDetailUser(Utils.state.detailUser);
                    })
                    .catch((e) => {
                        console.error(e);
                    })
            } catch (e) { console.error(e) }
        }
        openDetailTransaction();
        openDetailUser();

    }

    const doRefresh = (event: CustomEvent<RefresherEventDetail>) => {
        setState(state + 1);
        setTimeout(() => {
            event.detail.complete();
        }, 1000);
    }

    const doRefreshAfterUpdate = () => {
        setState(state + 1);
        setTimeout(() => {

        }, 1000);
    }

    const openAlert = (params: any) => {
        switch (params) {
            case "Delete":
                setButtonAlert(params);
                setMessage("Yakin Ingin Menghapus Transaksi Ini?");
                showAlert(true);
                break;
            case "Proccess":
                setUpdate({ ...update, status: 'Shipping' })
                setButtonAlert(params);
                setMessage("Proses Transaksi Ini?");
                showAlert(true);
                break;
            case "Success":
                setButtonAlert(params);
                setMessage("Berhasil");
                showAlert(true);
                break;
            default: break;
        }
    }
    return (
        <IonPage>
            <IonAlert isOpen={alert} onDidDismiss={() => showAlert(false)}
                message={message}
                buttons={buttonAlert === "Delete" ?
                    [{
                        text: 'Cancel',
                        role: 'cancel',
                    },
                    {
                        text: 'Hapus',
                        cssClass: 'danger',
                        handler: async () => {
                            showAlert(false);
                            setLoading(true);
                            setPopOver(true);
                            try {
                                try {
                                    detailTransaksi.forEach((data) => {
                                        data.isi.forEach(async (data2: any) => {
                                            await Utils.updateStock(data2.id, data2.jumlah);
                                        })
                                    })
                                } catch (e) {
                                    console.error(e);
                                }

                                await Utils.deleteTransactionUser(update.idUser, update.idTransaksi);
                                setMessage("Transaksi Berhasil Dihapus");
                                setLoading(false);
                                setPopOver(false);
                                setButtonAlert('Success')
                                showAlert(true);

                            } catch (e) {
                                setMessage("Terjadi Kesalahan \n" + e);
                                showAlert(true);
                            }
                        }

                    }
                    ] :

                    buttonAlert === "Proccess" ?
                        [{
                            text: 'Cancel',
                            role: 'cancel',
                            cssClass: 'secondary'
                        },
                        {
                            text: 'OK',
                            handler: async () => {
                                showAlert(false);
                                setLoading(true);
                                setPopOver(true);
                                try {
                                    await Utils.processTransaction(update.idUser, update.idTransaksi, update.status)
                                        .then(() => {
                                            setUpdate(defaultUpdate);
                                            setLoading(false);
                                            setPopOver(false);
                                            openAlert('Success');
                                        })
                                } catch (err) {
                                    console.error(err);
                                }
                            }
                        }
                        ] :
                        [{
                            text: 'OK',
                            handler: () => {
                                setModal(false);
                                doRefreshAfterUpdate();
                            }
                        }]
                }
            />
            <IonHeader>
                <IonToolbar color="tertiary">
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Transaksi</IonTitle>
                </IonToolbar>
            </IonHeader >
            <div className="dashboard"><h3>Transaksi</h3> Transaksi > Validasi Pembayaran</div>
            <IonItemDivider />

            <IonContent >
                <IonRefresher slot="fixed" onIonRefresh={doRefresh} />
                <IonGrid>
                    <IonRow>
                        {loading ? <IonSpinner name="circles" /> :
                            pending.length !== 0 ?
                                pending.map((item: any, index: any) => {
                                    return (
                                        <IonCol size={index === 0 ? "6" : "6"} key={index}>
                                            <IonCard color={!item.fotoBukti ? "warning" : "primary"}>
                                                <IonCardContent>
                                                    <IonText color="dark" style={{ fontSize: 10 }}>
                                                        Tanggal Transaksi : <br />
                                                        {new Date(item.date.toDate()).toLocaleString()}
                                                    </IonText>
                                                    <br />
                                                    <IonText color="dark" style={{ fontSize: 10 }}>
                                                        Status : {item.status === "Pending" ? "Menunggu Pembayaran" : "Menunggu Validasi"}
                                                    </IonText>
                                                </IonCardContent>
                                                <IonButton color="light"
                                                    onClick={() => openModal(item.idUser, item.idTransaksi)}>
                                                    Lihat
                                                </IonButton>

                                            </IonCard>
                                        </IonCol>
                                    )
                                }) :
                                <IonCol>
                                    <IonCard>
                                        <IonCardHeader>
                                            <IonCardTitle>Tidak Ada Transaksi</IonCardTitle>
                                        </IonCardHeader>
                                    </IonCard>
                                </IonCol>
                        }
                    </IonRow>
                </IonGrid>
                <IonModal isOpen={modal} onDidDismiss={() => setModal(false)}>
                    <IonToolbar>
                        <IonButtons>
                            <IonTitle>Validasi Pembayaran</IonTitle>
                            <IonButton onClick={() => setModal(false)}>
                                <IonIcon icon={close} />
                                <IonLabel>Close</IonLabel>
                            </IonButton>
                        </IonButtons>
                    </IonToolbar>
                    <IonContent>

                        <IonGrid>
                            {loading ?
                                <IonPopover isOpen={popOver}>
                                    <IonLabel>Please Wait <IonSpinner name="circles" /></IonLabel>
                                </IonPopover> : ''
                            }

                            <IonRow>
                                {detailTransaksi.map((item, index) => {
                                    return (
                                        <IonCol key={index}>
                                            <IonRow style={{ fontSize: 12 }}>
                                                <IonCol size="5">
                                                    ID Transaksi<br />
                                                    Tanggal Transaksi<br />
                                                    Nama Pembeli<br />
                                                    Alamat Pengiriman<br />
                                                </IonCol>
                                                <IonCol size="1">
                                                    :<br />
                                                    :<br />
                                                    :<br />
                                                    :<br />
                                                </IonCol>
                                                <IonCol style={{ float: 'left' }}>
                                                    {item.idTransaksi}<br />
                                                    {new Date(item.date.toDate()).toLocaleString()}<br />
                                                    {item.nama}<br />
                                                    {item.alamat}<br />
                                                </IonCol>
                                            </IonRow>
                                            <IonRow style={{ fontSize: 12 }}>
                                                <IonCol size="5">
                                                    No. Telpon / Hp<br />
                                                    Total Belanja<br />
                                                    Status<br />
                                                    Foto Bukti<br />
                                                </IonCol>
                                                <IonCol size="1">
                                                    :<br />
                                                    :<br />
                                                    :<br />
                                                    :<br />
                                                </IonCol>
                                                <IonCol style={{ float: 'left' }}>
                                                    {item.tlp}<br />
                                                    Rp. {item.total.toLocaleString()}<br />
                                                    {item.status === "Pending" ? "Menunggu Pembayaran" : "Menunggu Validasi"}<br />
                                                    <img alt="" src={item.fotoBukti} style={{ width: 100, }}
                                                        onClick={() => setPopOverImage(true)} />
                                                </IonCol>
                                            </IonRow>

                                            <IonPopover isOpen={popOverImage} onDidDismiss={() => setPopOverImage(false)}>
                                                <img src={item.fotoBukti} alt="" />
                                            </IonPopover>

                                            <IonItemDivider />
                                            <IonText>Detail Transaksi</IonText><br />
                                            <IonCol key={index} size="3">
                                                <IonText style={{ fontSize: 10, }}>
                                                    <table style={{ width: 350 }}>
                                                        <thead>
                                                            <tr style={{ borderBottom: "1px solid" }}>
                                                                <td>No</td>
                                                                <td>Nama Barang</td>
                                                                <td>Qty</td>
                                                                <td>Harga</td>
                                                                <td>Total /barang</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {item.isi.map((items: any, index: any) => {
                                                                var no = index + 1;
                                                                return (
                                                                    <tr>
                                                                        <td>{no}</td>
                                                                        <td style={{ width: 150 }}>{items.nama}</td>
                                                                        <td>{items.jumlah}</td>
                                                                        <td>Rp. {items.harga.toLocaleString()}</td>
                                                                        <td>Rp. {items.hargaTotal.toLocaleString()}</td>
                                                                    </tr>

                                                                );
                                                            })}
                                                            <tr>
                                                                <td colSpan={5}><IonItemDivider color="default" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td colSpan={4}>Grand Total</td>
                                                                <td>Rp. {item.total.toLocaleString()}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </IonText>
                                            </IonCol>
                                            <br />
                                            <IonButton color="danger" onClick={() => openAlert("Delete")}>Cancel</IonButton>
                                            <IonButton color="success" onClick={() => openAlert("Proccess")}>Process</IonButton>

                                        </IonCol>
                                    )
                                })}
                            </IonRow>
                        </IonGrid>

                    </IonContent>
                </IonModal>
            </IonContent>
        </IonPage>
    );
};

export default ValidatePage;