import { useParams, useHistory } from "react-router";
import React, { useState, useEffect } from "react";
import Utils from "../../Utils";
import {
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonSpinner,
  IonPage,
  IonToolbar,
  IonButtons,
  IonBackButton,
  IonContent,
  IonLabel,
  IonText,
  IonButton,
  IonList,
  IonItem,
  IonInput,
  IonSelect,
  IonSelectOption,
  IonTextarea,
  IonHeader,
  IonTitle,
  IonAlert,
  IonRow,
  IonCol,
  IonGrid,
  IonProgressBar,
  IonIcon,
  IonPopover,
} from "@ionic/react";
import { returnLeft, trash, cloudUpload } from "ionicons/icons";
import firebase from 'firebase';

const EditProduct: React.FC = () => {
  let history = useHistory();
  let { id } = useParams();
  const [uploadProgress, setUploadProgress] = useState(Number);
  const [progressBar, setProgressBar] = useState(false);
  const [popOver, setPopOver] = useState(false);
  const [loadingEdit, setLoadingEdit] = useState(false);
  const [loading, setLoading] = useState(true);

  const [state] = useState(0);
  const [alert, setAlert] = useState(false);
  const [status, setStatus] = useState("");
  const [message, setMessage] = useState("");
  const [newData, setNewData] = useState<any>({
    id: 0,
    nama: '',
    harga: 0,
    kategori: '',
    jenis: '',
    penjual: '',
    desc: '',
    stok: 0,
    foto: '',
    noHp: '',
    norek: '',
  });
  const [newFoto, setNewFoto] = useState({
    foto: null,
  });

  useEffect(() => {
    async function getDataDetails() {
      await Utils.getDetails(id);
      setLoading(false);
      setNewData(Utils.state.details[0]);
    }
    getDataDetails();
  }, [state, id]);

  const handleSubmitEditForm = (e: any) => {
    e.preventDefault();
    async function onSubmit() {
      setLoadingEdit(true);
      setPopOver(true);
      try {
        await Utils.editProduct(
          id,
          newData.nama,
          newData.harga,
          newData.kategori,
          newData.jenis,
          newData.penjual,
          newData.desc,
          newData.stok,
          newData.noHp,
          newData.norek,
        );
        setStatus("Success");
        setMessage("Update Data Berhasil");
        setAlert(true);
      } catch (e) {
        setStatus("Fail");
        setMessage("Gagal Update Data");
        setAlert(true);
      }
      setPopOver(false);
      setLoadingEdit(false);
    }
    onSubmit();
  }

  const deletePhoto = (e: any, a: any) => {
    e.preventDefault();
    const submitDelete = async () => {
      try {
        await Utils.strg()
          .refFromURL(newData.foto)
          .delete()
          .then(() => {
            setUploadProgress(0);
            setNewData({ ...newData, foto: null });
          })

      } catch (e) {
        console.error(e);
      }

    }
    submitDelete();
  }

  const uploadPhoto = (e: any, a: any) => {
    e.preventDefault();
    async function onUpload() {
      try {
        const uploadTask = Utils.strg().ref(`images/${a.name}`).put(a);
        setProgressBar(true);
        uploadTask.on(
          firebase.storage.TaskEvent.STATE_CHANGED,
          snapshot => {
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            setUploadProgress(progress);
          },
          error => {
            setMessage(error.message);
            setAlert(true);
          },
          async () => {
            await Utils.strg()
              .ref('images')
              .child(a.name)
              .getDownloadURL()
              .then(async (url) => {
                await Utils.editFotoProduk(newData.id, url).then(() => {
                  setNewData({ ...newData, foto: url });
                  setMessage("Berhasil Mengupload File");
                  // setStatusUpload(true);
                  setProgressBar(false);
                })
              })
          }
        );

      } catch (e) {

      }
    }
    onUpload();
  }

  function waitContent() {
    return (
      <IonCard>
        <IonCardHeader className="ListHeaderDetail">
          <IonCardTitle>
          </IonCardTitle>
        </IonCardHeader>
        <IonCardContent>
          {loading ? <IonSpinner name="circles" /> : ''}
        </IonCardContent>
      </IonCard>
    )
  }

  return (
    <IonPage>
      <IonContent fullscreen>
        <IonAlert isOpen={alert} message={message} onDidDismiss={() => setAlert(false)}
          buttons={
            status === "Success" ?
              [{
                text: 'OK',
                handler: () => {
                  setAlert(false);
                  history.replace("/Product");
                }
              }] :
              [{
                text: 'OK',
                handler: () => {
                  setAlert(false);
                }
              }]
          }
        />

        <IonHeader>
          <IonToolbar color="tertiary">
            <IonButtons>
              <IonBackButton icon={returnLeft} color="danger" defaultHref="/Product" />
              <IonTitle>Edit Produk</IonTitle>
            </IonButtons>
          </IonToolbar>
        </IonHeader>

        {loading ? waitContent() :
          
          <IonList lines="full" class="ion-no-margin ion-no-padding">
            
            {loadingEdit ? 
            <IonPopover isOpen={popOver}>
              <IonLabel>Please wait<IonSpinner name="circles" /></IonLabel>
            </IonPopover> 
            : ''
            }

            <form onSubmit={newData.foto ? (event) => deletePhoto(event, newData.id) : (event) => uploadPhoto(event, newFoto.foto)}>
              <IonItem>
                <IonLabel position="stacked">Foto</IonLabel>
                <IonGrid>
                  <IonRow>
                    <IonCol size="6">
                      {newData.foto ?
                        <img src={newData.foto} alt="" />
                        :

                        progressBar ?
                          <IonCol>
                            <IonText>Uploading Data {uploadProgress}%</IonText> <br />
                            <IonProgressBar value={uploadProgress}></IonProgressBar>
                          </IonCol>
                          :

                          uploadProgress === 100 ?
                            message
                            :
                            <IonCol>
                              <input type="file" onChange={(e: any) => { setNewFoto({ ...newData, foto: e.target.files[0] }) }} />
                            </IonCol>
                      }
                    </IonCol>
                    <IonCol style={{ marginTop: 50 }}>
                      {newData.foto ?
                        <IonButton color="danger" type="submit">
                          <IonIcon icon={trash} />
                        </IonButton>
                        :
                        <IonButton color="primary" type="submit">
                          <IonIcon icon={cloudUpload} />
                        </IonButton>
                      }
                    </IonCol>
                  </IonRow>
                </IonGrid>
              </IonItem>
            </form>
            <form onSubmit={(event) => handleSubmitEditForm(event)}>

              <IonItem>
                <IonLabel position="stacked">Nama Produk<IonText color="danger">*</IonText></IonLabel>
                <IonInput required type="text" value={newData.nama}
                  onIonChange={(e) => setNewData({ ...newData, nama: e.detail.value! })} />
              </IonItem>
              <IonItem>
                <IonLabel>Kategori</IonLabel>
                <IonSelect interface="popover" placeholder="Pilihan" value={newData.kategori}
                  onIonChange={(e) => setNewData({ ...newData, kategori: e.detail.value! })}>
                  <IonSelectOption selected={newData.kategori === "buah" ? true : false}
                    value="buah">Buah-buahan</IonSelectOption>
                  <IonSelectOption selected={newData.kategori === "sayur" ? true : false}
                    value="sayur">sayur</IonSelectOption>
                </IonSelect>
              </IonItem>
              <IonItem>
                <IonLabel position="fixed">Jenis</IonLabel>
                <IonInput required value={newData.jenis} onIonChange={(e) => setNewData({ ...newData, jenis: e.detail.value! })} />
              </IonItem>
              <IonItem>
                <IonLabel position="fixed">Stok</IonLabel>
                <IonInput required type="number" value={!newData.stok ? 0 : newData.stok} min="0" 
                onIonChange={(e) => setNewData({ ...newData, stok: parseInt(e.detail.value!) })} />
              </IonItem>
              <IonItem>
                <IonLabel position="fixed">Harga</IonLabel>
                Rp. <IonInput required type="number" value={!newData.harga ? 0 : newData.harga} min="0"
                  onIonChange={(e) => setNewData({ ...newData, harga: parseInt(e.detail.value!) })} />
              </IonItem>
              <IonItem>
                <IonLabel position="stacked" >Kota</IonLabel>
                <IonInput required type="text" value={newData.penjual}
                  onIonChange={(e) => setNewData({ ...newData, penjual: e.detail.value! })} />
              </IonItem>
              <IonItem>
                <IonLabel position="stacked" >No. Telepon</IonLabel>
                <IonInput required type="text" value={newData.noHp}
                  onIonChange={(e) => setNewData({ ...newData, noHp: e.detail.value! })} />
              </IonItem>
              <IonItem>
                <IonLabel position="stacked" >No. Rekening</IonLabel>
                <IonInput required type="text" value={newData.norek}
                  onIonChange={(e) => setNewData({ ...newData, norek: e.detail.value! })} />
              </IonItem>
              <IonItem>
                <IonLabel position="stacked">Deskripsi</IonLabel>
                <IonTextarea required value={newData.desc}
                  onIonChange={(e) => setNewData({ ...newData, desc: e.detail.value! })}></IonTextarea>
              </IonItem>
              <div className="ion-padding">
                <IonButton expand="block" type="submit" class="ion-no-margin">submit</IonButton>
              </div>
            </form>
          </IonList>
        }
      </IonContent>
    </IonPage>
  );
}

export default EditProduct;