import {
    IonButtons,
    IonCard,
    IonCardContent,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonText,
    IonGrid,
    IonRow,
    IonSpinner,
    IonCol,
    IonButton,
    IonModal,
    IonIcon,
    IonLabel,
    IonList,
    IonItemDivider,
    IonRefresher,
    IonCardHeader,
    IonCardTitle,
    IonPopover,
} from '@ionic/react';
import { RefresherEventDetail } from '@ionic/core';
import { close } from 'ionicons/icons';
import React, { useState, useEffect } from 'react';
import './Dashboard.css';
import Utils from '../../Utils';

const OrdersPage: React.FC = () => {
    const [loading, setLoading] = useState(false);
    const [transaksi, setTransaksi] = useState<any[]>([]);
    const [state, setState] = useState(0);
    const [modal, setModal] = useState(false);
    const [detailTransaksi, setDetailTransaksi] = useState<any[]>([]);
    const [detailUser, setDetailUser] = useState<any[]>([]);
    const [popOver, setPopOver] = useState(false);

    useEffect(() => {
        async function showTransaction() {
            setLoading(true);
            try {
                await Utils.getTransactionUsers()
                    .then(() => {

                        setTimeout(() => {
                            setTransaksi(transaksi.splice(0, 100));
                        }, 1000);

                        setTimeout(() => {
                            setTransaksi(Utils.state.transaction);
                            setLoading(false);
                        }, 1500);
                    })

            } catch (err) {
                console.error(err);
            }
        }
        showTransaction();
    }, [state]);

    const openModal = (a: any, b: any) => {
        setModal(true);
        const openDetailTransaction = async () => {
            detailTransaksi.splice(0, 100);
            try {
                await Utils.getDetailTransactionUser(a, b)
                    .then(() => {
                        setDetailTransaksi(Utils.state.detailTransaksi);

                    })
                    .catch((e) => {
                        console.error(e);
                    })
            } catch (e) { }
        }

        const openDetailUser = async () => {
            detailUser.splice(0, 100);
            try {
                await Utils.getDetailUser(a)
                    .then(() => {
                        setDetailUser(Utils.state.detailUser);
                    })
                    .catch((e) => {
                        console.error(e);
                    })
            } catch (e) { console.error(e) }
        }
        openDetailTransaction();
        openDetailUser();
    }

    const doRefresh = (event: CustomEvent<RefresherEventDetail>) => {
        setState(state + 1);
        setTimeout(() => {
            event.detail.complete();
        }, 1000);
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar color="tertiary">
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Transaksi</IonTitle>
                </IonToolbar>
            </IonHeader >
            <div className="dashboard"><h3>Transaksi</h3> Transaksi > Penjualan</div>
            <IonItemDivider />
            <IonContent >
                <IonRefresher slot="fixed" onIonRefresh={doRefresh} />
                <IonGrid>
                    <IonRow>
                        {loading ? <IonSpinner name="circles" /> :
                            transaksi.length !== 0 ?
                                transaksi.map((item: any, index: any) => {
                                    return (
                                        <IonCol size={index === 0 ? "6" : "6"} key={index}>
                                            <IonCard color={!item.fotoBukti ? "warning" : "success"}>
                                                <IonCardContent>
                                                    <IonText color="dark" style={{ fontSize: 10 }}>
                                                        Tanggal Transaksi : <br />
                                                        {new Date(item.date.toDate()).toLocaleString()}
                                                    </IonText>
                                                    <br />
                                                    <IonText color="dark" style={{ fontSize: 10 }}>
                                                        Status : {item.status}
                                                    </IonText>
                                                </IonCardContent>
                                                <IonButton color="light"
                                                    onClick={() => openModal(item.idUser, item.idTransaksi)}>
                                                    Lihat
                                                </IonButton>

                                            </IonCard>
                                        </IonCol>
                                    )
                                }) :
                                <IonCol>
                                    <IonCard>
                                        <IonCardHeader>
                                            <IonCardTitle>Tidak Ada Transaksi</IonCardTitle>
                                        </IonCardHeader>
                                    </IonCard>
                                </IonCol>
                        }
                    </IonRow>
                </IonGrid>

                <IonModal isOpen={modal} onDidDismiss={() => setModal(false)}>
                    <IonToolbar>
                        <IonButtons>
                            <IonTitle>Transaksi</IonTitle>
                            <IonButton onClick={() => setModal(false)}>
                                <IonIcon icon={close} />
                                <IonLabel>Close</IonLabel>
                            </IonButton>
                        </IonButtons>
                    </IonToolbar>
                    <IonContent>

                        <IonList>
                            <IonGrid>
                                <IonRow>
                                    {detailTransaksi.map((item, index) => {
                                        return (
                                            <IonCol key={index}>
                                                <IonRow style={{ fontSize: 12 }}>
                                                    <IonCol size="5">
                                                        ID Transaksi<br />
                                                        Tanggal Transaksi<br />
                                                        Nama Pembeli<br />
                                                        Alamat Pengiriman<br />
                                                    </IonCol>
                                                    <IonCol size="1">
                                                        :<br />
                                                        :<br />
                                                        :<br />
                                                        :<br />
                                                    </IonCol>
                                                    <IonCol style={{ float: 'left' }}>
                                                        {item.idTransaksi}<br />
                                                        {new Date(item.date.toDate()).toLocaleString()}<br />
                                                        {item.nama}<br />
                                                        {item.alamat}<br />
                                                    </IonCol>
                                                </IonRow>
                                                <IonRow style={{ fontSize: 12 }}>
                                                    <IonCol size="5">
                                                        No. Telpon / Hp<br />
                                                        Total Belanja<br />
                                                        Status<br />
                                                        Foto Bukti<br />
                                                    </IonCol>
                                                    <IonCol size="1">
                                                        :<br />
                                                        :<br />
                                                        :<br />
                                                        :<br />
                                                    </IonCol>
                                                    <IonCol style={{ float: 'left' }}>
                                                        {item.tlp}<br />
                                                        Rp. {item.total.toLocaleString()}<br />
                                                        {item.status === "Shipping" ? "Dalam Pengiriman" : "Diterima"}<br />
                                                        <img alt="" src={item.fotoBukti} style={{ width: 100, }}
                                                            onClick={() => setPopOver(true)} />
                                                    </IonCol>
                                                </IonRow>

                                                <IonPopover isOpen={popOver} onDidDismiss={() => setPopOver(false)}>
                                                    <img src={item.fotoBukti} alt="" />
                                                </IonPopover>

                                                <IonItemDivider />
                                                <IonText>Detail Transaksi</IonText><br />
                                                <IonCol key={index} size="3">
                                                    <table style={{ width: 350, fontSize: 10 }}>
                                                        <thead>
                                                            <tr style={{ borderBottom: "1px solid" }}>
                                                                <td>No</td>
                                                                <td>Nama Barang</td>
                                                                <td>Qty</td>
                                                                <td>Harga</td>
                                                                <td>Total /barang</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {item.isi.map((items: any, index: any) => {
                                                                var no = index + 1;
                                                                return (
                                                                    <tr>
                                                                        <td>{no}</td>
                                                                        <td style={{ width: 150 }}>{items.nama}</td>
                                                                        <td>{items.jumlah}</td>
                                                                        <td>Rp. {items.harga.toLocaleString()}</td>
                                                                        <td>Rp. {items.hargaTotal.toLocaleString()}</td>
                                                                    </tr>

                                                                );
                                                            })}
                                                            <tr>
                                                                <td colSpan={5}><IonItemDivider color="default" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td colSpan={4}>Grand Total</td>
                                                                <td>Rp. {item.total.toLocaleString()}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </IonCol>
                                            </IonCol>
                                        )
                                    })}
                                </IonRow>
                            </IonGrid>
                        </IonList>
                    </IonContent>
                </IonModal>
            </IonContent>
        </IonPage>
    );
};

export default OrdersPage;