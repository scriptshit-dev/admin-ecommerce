import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton,
  IonIcon,
  IonGrid,
  IonRow,
  IonList,
  IonAlert,
  IonSpinner,
  IonCard,
  IonCardContent,
  IonModal,
  IonLabel,
  IonItem,
  IonText,
  IonInput,
  IonSelect,
  IonSelectOption,
  IonTextarea,
  IonCol,
  IonProgressBar,
  IonRefresher,
  IonCardHeader,
  IonCardTitle,
  IonPopover,
} from '@ionic/react';
import { RefresherEventDetail } from '@ionic/core';
import React, { useState, useEffect } from 'react';
import { add, trash, create, cash, close, cloudUpload } from 'ionicons/icons';
import './Product.css';
import Utils from '../../Utils';
import firebase from 'firebase';

interface input {
  nama?: string,
  harga?: number,
  kategori?: string,
  jenis?: string,
  penjual?: string,
  desc?: string,
  foto?: null,
  stok?: number,
  noHp?: string,
  norek?: string,
  an?: string
}

const inputInput = {
  nama: '',
  harga: 0,
  kategori: '',
  jenis: '',
  penjual: '',
  desc: '',
  foto: null,
  stok: 0,
  noHp: '',
  norek: '',
  an: '',
}

const ProductPage: React.FC = () => {
  const [status, setStatus] = useState("");

  const [statusUpload, setStatusUpload] = useState(false);
  const [uploadProgress, setUploadProgress] = useState(Number);
  const [progressBar, setProgressBar] = useState(false);
  const [popOver, setPopOver] = useState(false);
  const [loadingAdd, setLoadingAdd] = useState(false);
  const [loading, setLoading] = useState(true);

  const [modal, setModal] = useState(false);
  const [alert, showAlert] = useState(false);
  const [message, setMessage] = useState('');
  const [data, setData] = useState<any[]>([]);
  const [state, setState] = useState(0);
  const [idProduk, setIdProduk] = useState();
  const [urlDelete, setUrlDelete] = useState();
  const [urlFoto, setUrlFoto] = useState('');
  const [inputValue, setInputValue] = useState<input>({
    nama: '',
    harga: 0,
    kategori: '',
    jenis: '',
    penjual: '',
    desc: '',
    foto: null,
    stok: 0,
    noHp: '',
    norek: '',
    an : '',
  });

  const uploadFoto = (e: any, a: any) => {
    e.preventDefault();
    async function onUpload() {
      try {
        const uploadTask = Utils.strg().ref(`images/${a.name}`).put(a);
        setProgressBar(true);
        uploadTask.on(
          firebase.storage.TaskEvent.STATE_CHANGED,
          snapshot => {
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            setUploadProgress(progress);
          },
          error => {
            setMessage(error.message);
            showAlert(true);
          },
          async () => {
            await Utils.strg()
              .ref('images')
              .child(a.name)
              .getDownloadURL()
              .then(url => {
                setMessage("Berhasil Mengupload File");
                setUrlFoto(url);
                setStatusUpload(true);
                setProgressBar(false);
              })
          }
        );

      } catch (e) {

      }
    }
    onUpload();
  }

  const hapusFoto = (e: any, a: any) => {
    e.preventDefault();
    function handleDeleteFoto() {
      try {
        let storageRef = Utils.strg().ref(`images/${a.name}`);
        storageRef.delete().then(function () {
          setUploadProgress(0);
          setStatus('');
          setInputValue({ ...inputValue, foto: null });
          setUrlFoto('');
          setStatusUpload(false);
          setMessage("Berhasil Menghapus Foto");
          showAlert(true);
        })
          .catch((e) => {
            setStatus('');
            showAlert(true);
            setMessage(e.message);
          })
      } catch (e) {
        setStatus('');
        showAlert(true);
        setMessage(e);
      }
    }
    handleDeleteFoto();
  }

  useEffect(() => {
    async function show() {
      try {
        setLoading(true);
        setData(data.splice(0, 100))
        await Utils.getBarang()
          .then(() => {
            setData(Utils.state.list);
            setLoading(false);
          })
          .catch(() => {
            setStatus("Fails Database");
            setMessage(Utils.state.error);
            showAlert(true);

          })

      } catch (e) {
        setStatus("Fails Database");
        setMessage(Utils.state.error)
        showAlert(true);
      }
    }
    show();
  }, [state]);

  const doRefreshContent = () => {
    setState(state + 1);
    setTimeout(() => {

    }, 1000);
  }

  const doRefresh = (event: CustomEvent<RefresherEventDetail>) => {
    setState(state + 1);
    setTimeout(() => {
      event.detail.complete();
    }, 1000);
  }

  function handleDelete(a: any, b: any,c:any) {
    setIdProduk(a);
    setUrlDelete(c);
    setStatus("Delete");
    setMessage("Hapus " + b + " ?");
    showAlert(true);
  };

  const handleSubmitForm = (e: any) => {
    e.preventDefault();
    async function onSubmit() {
      setLoadingAdd(true);
      setPopOver(true);
      try {
        if (!inputValue.kategori) {
          setStatus("Error");
          setMessage("Kategori Tidak Boleh Kosong!");
          showAlert(true);
        } else if (!inputValue.harga) {
          setStatus("Error");
          setMessage("Harga Produk Tidak Boleh 0");
          showAlert(true);
        } else {
          await Utils.addProduct(
            inputValue.nama,
            inputValue.harga,
            inputValue.kategori,
            inputValue.jenis,
            inputValue.penjual,
            inputValue.desc,
            urlFoto,
            inputValue.stok,
            inputValue.noHp,
            inputValue.norek+" A/N "+inputValue.an,
          )
            .then(() => {
              setInputValue(inputInput);
              setStatus("Success Add Data");
              setMessage("Data Berhasil Ditambahkan");
              setModal(false);
              showAlert(true);
            })
            .catch((e) => {
              setStatus("Fail Add Data");
              setMessage(e.message);
              showAlert(true);
            })
        }
      } catch (e) {
        setStatus('');
        showAlert(true);
        setMessage(e);
      };
      setPopOver(false);
      setLoadingAdd(false);
    }
    onSubmit();
  }

  return (
    <IonPage className="dashboard-page">
      <IonAlert
        isOpen={alert}
        message={message}
        onDidDismiss={() => {
          showAlert(false);
        }}
        buttons={
          status === "Success Add Data" ?
            [{
              text: 'OK',
              handler: () => {
                showAlert(false);
                setModal(false);
                doRefreshContent();
              }
            }] :

            status === "Success Delete" ?
              [{
                text: 'OK',
                handler: () => {
                  showAlert(false);
                  doRefreshContent();
                }
              }] :

              status === "Fail Add Data" ?
                [{
                  text: 'OK',
                  handler: () => {
                    showAlert(false);
                  }
                }] :

                status === "Fail Delete" ?
                  [{
                    text: 'OK',
                    handler: () => {
                      showAlert(false);
                    }
                  }] :

                  status === "Fails Database" ?
                    [{
                      text: 'OK',
                      handler: () => {
                        showAlert(false);
                        doRefreshContent();
                      }
                    }] :

                    status === "Delete" ?
                      [{
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary'
                      },
                      {
                        text: 'Okay',
                        handler: () => {
                          setLoading(true);
                          async function confirmDelete() {
                            try {
                              await Utils.deleteProduk(idProduk,urlDelete);
                              setStatus("Success Delete");
                              setMessage("Berhasil Menghapus Produk");
                              showAlert(true);
                            } catch (e) {
                              setStatus("Fail Delete");
                              setMessage("Gagal Menghapus Produk");
                              showAlert(true);
                            }
                          }
                          setLoading(false);
                          confirmDelete();
                        }
                      }] :
                      [{
                        text: 'OK',
                        handler: () => {
                          showAlert(false);
                        }
                      }]
        } />
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Katalog</IonTitle>
        </IonToolbar>
      </IonHeader >
      <div className="dashboard"><h2>Produk</h2> Katalog > Produk</div>
      <IonButton color="success" disabled={!loading ? false : true} onClick={() => setModal(true)}>
        <IonIcon size="small" icon={add} />Tambah Produk
      </IonButton>
      <IonContent>
        <IonRefresher slot="fixed" onIonRefresh={doRefresh} />
        <IonModal isOpen={modal} backdropDismiss={false} onDidDismiss={() => setModal(false)}>
          <IonToolbar>
            <IonButtons>
              <IonTitle>Tambah Produk</IonTitle>
              <IonButton onClick={() => setModal(false)}>
                <IonIcon icon={close} />
                <IonLabel>Close</IonLabel>
              </IonButton>
            </IonButtons>
          </IonToolbar>
          <IonContent>
            <IonList lines="full">
            {loadingAdd ? 
            <IonPopover isOpen={popOver}>
              <IonLabel>Please wait<IonSpinner name="circles" /></IonLabel>
            </IonPopover> 
            : ''
            }
              <form onSubmit=
                {!statusUpload ? (event) => uploadFoto(event, inputValue.foto)
                : 
                (event) => hapusFoto(event, inputValue.foto)}>

                <IonItem>
                <IonLabel position="stacked">Foto Produk</IonLabel>
                  <IonGrid>
                    <IonRow>
                    {progressBar ?
                        <IonCol>
                          <IonText>Uploading Data {uploadProgress}%</IonText> <br />
                          <IonProgressBar value={uploadProgress}></IonProgressBar>
                        </IonCol>
                      : 

                      uploadProgress === 100 ? 
                        // message
                        <IonCol>
                          <img src={urlFoto} alt=""/>
                        </IonCol>
                      :
                        <IonCol>
                          <input type="file" disabled={statusUpload ? true : false}
                          onChange={(e: any) => { setInputValue({ ...inputValue, foto: e.target.files[0] }) }}/> 
                        </IonCol>
                    }
                      <IonCol>
                        {statusUpload ? 
                          <IonButton type="submit" disabled={false} color="danger">
                            <IonIcon icon={trash}/>
                          </IonButton> 
                          : 
                          <IonButton type="submit" disabled={!inputValue.foto || progressBar ? true : false} color="primary">
                            <IonIcon icon={cloudUpload}/>
                          </IonButton>
                        }
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </IonItem>
              </form>

              <form onSubmit={(event) => handleSubmitForm(event)}>
                <IonItem>
                  <IonLabel position="stacked">Nama Produk</IonLabel>
                  <IonInput required type="text" value={inputValue.nama}
                    onIonChange={(e) => setInputValue({ ...inputValue, nama: e.detail.value! })} />
                </IonItem>
                <IonItem>
                  <IonLabel>Kategori</IonLabel>
                  <IonSelect interface="action-sheet" interfaceOptions={{ translucent: true }}
                    placeholder="Pilih" value={inputValue.kategori}
                    onIonChange={(e) => setInputValue({ ...inputValue, kategori: e.detail.value! })}>
                    <IonSelectOption value="buah">Buah-buahan</IonSelectOption>
                    <IonSelectOption value="sayur">Sayur</IonSelectOption>
                  </IonSelect>
                </IonItem>
                <IonItem>
                  <IonLabel position="stacked">Jenis</IonLabel>
                  <IonInput required type="text" value={inputValue.jenis}
                    onIonChange={(e) => setInputValue({ ...inputValue, jenis: e.detail.value! })} />
                </IonItem>
                <IonItem>
                  <IonLabel position="fixed">Harga</IonLabel>
                  Rp. <IonInput required type="number" min="0" value={`${inputValue.harga}`}
                    onIonChange={(e) => setInputValue({ ...inputValue, harga: parseInt(e.detail.value!) })} />
                </IonItem>
                <IonItem>
                  <IonLabel position="fixed">Stok</IonLabel>
                  <IonInput required type="number" min="0" value={`${inputValue.stok}`}
                  onIonChange={(e) => setInputValue({ ...inputValue, stok: parseInt(e.detail.value!) })} />
                </IonItem>
                <IonItem>
                  <IonLabel position="stacked" >Kota</IonLabel>
                  <IonInput required type="text" value={inputValue.penjual}
                    onIonChange={(e) => setInputValue({ ...inputValue, penjual: e.detail.value! })} />
                </IonItem>
                <IonItem>
                  <IonLabel position="stacked" >No. Telepon</IonLabel>
                  <IonInput required type="text" value={inputValue.noHp}
                    onIonChange={(e) => setInputValue({ ...inputValue, noHp: e.detail.value! })} />
                </IonItem>
                <IonGrid>
                  <IonRow>
                    <IonCol size="5">
                      <IonItem>
                        <IonLabel position="stacked" >No. Rekening</IonLabel>
                        <IonInput required type="text" value={inputValue.norek}
                          onIonChange={(e) => setInputValue({ ...inputValue, norek: e.detail.value! })} />
                      </IonItem>
                    </IonCol>
                    <IonCol>
                      <IonItem>
                        <IonLabel position="stacked" >Atas Nama</IonLabel>
                        <IonInput required type="text" value={inputValue.an}
                          onIonChange={(e) => setInputValue({ ...inputValue, an: e.detail.value! })} />
                      </IonItem>
                    </IonCol>
                  </IonRow>
                </IonGrid>
                <IonItem>
                  <IonLabel position="stacked">Deskripsi</IonLabel>
                  <IonTextarea id="deskripsi" value={inputValue.desc}
                    onIonChange={(e) => setInputValue({ ...inputValue, desc: e.detail.value! })} ></IonTextarea>
                </IonItem>
                <div className="ion-padding">
                  <IonButton expand="block" type="submit" class="ion-no-margin"
                    disabled={!urlFoto ? true : false}>submit</IonButton>
                </div>
              </form>
            </IonList>
          </IonContent>
        </IonModal>
        <IonGrid>
          <IonRow>
            {loading ? <IonSpinner name="circles" /> :
              data.length !== 0 ?
                data.map((item, index) => {
                  return (
                    <IonCol size={index === 0 ? "6" : "6"} key={index}>
                      <IonCard>
                        <img src={item.foto} alt="" className="badanProduk" />
                        <IonCardContent>
                          <IonText color="dark" className="font">
                            Nama : {item.nama}
                          </IonText>
                          <br />
                          <IonText color="dark" className="font">
                            Kota : {item.penjual}
                          </IonText>
                          <br />
                          <IonText color="dark" className="font">
                            Stok : {item.stok}
                          </IonText>
                          <br />
                          <IonText color="dark" className="font">
                            Harga :
                        </IonText>
                          <IonText color="dark" className={item.harga.toString().length === 4 ? "font2" : "font3"}>
                            <IonIcon color="success" icon={cash} className="cash-icon" />{' '}
                            Rp. {item.harga.toLocaleString()}
                          </IonText>
                          <br />
                          <IonText color="dark" className="font">
                            Deskripsi : <br />{item.desc}
                          </IonText>
                        </IonCardContent>
                        <IonButtons>
                          <IonButton href={"/EditProduct/" + item.id} className="button" size="small" color="primary">Edit
                          <IonIcon size="small" icon={create} />
                          </IonButton>
                          <IonButton color="danger" onClick={() => handleDelete(item.id, item.nama,item.foto)} size="small">Delete
                          <IonIcon size="small" icon={trash} />
                          </IonButton>
                        </IonButtons>
                      </IonCard>
                    </IonCol>
                  )
                }) :
                <IonCard>
                  <IonCardHeader>
                    <IonCardTitle>Belum Ada Produk Yang Ditambahkan.<br /></IonCardTitle>
                  </IonCardHeader>
                </IonCard>
            }
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default ProductPage;