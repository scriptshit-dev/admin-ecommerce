import {
    IonButtons,
    IonContent,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar,
    IonSpinner,
    IonItemDivider,
    IonItem,
    IonGrid,
    IonRow,
    IonCol,
    IonCard,
} from '@ionic/react';
import React, { useState, useEffect } from 'react';
import Utils from '../../Utils';

const User: React.FC = () => {
    const [loading, setLoading] = useState(true);
    const [user, setUser] = useState<any[]>([]);
    const [state] = useState(0);

    useEffect(() => {
        setLoading(true);
        async function showUser() {
            try {
                setUser(user.splice(0, 100));
                await Utils.getUser();
                setUser(Utils.state.user);
                setLoading(false);
            } catch (e) {
                console.error(e);
            }
        }
        showUser();
    }, [state]);

    return (
        <IonPage className="dashboard-page">
            <IonHeader>
                <IonToolbar color="tertiary">
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Katalog</IonTitle>
                </IonToolbar>
            </IonHeader >
            <div className="dashboard"><h2>Pengguna</h2> Katalog > Daftar Pengguna</div>
            <IonItemDivider />
            <IonContent>
                <IonGrid>
                    <IonRow>
                        <IonCol>
                            {loading ? <IonSpinner name="circles" /> :
                                user.map((item, index) => {
                                    
                                    return (
                                        <IonCard color="primary">
                                        <IonRow style={{ fontSize: 12 }} key={index}>
                                            
                                            <IonCol size="5">
                                                Nama Pengguna<br />
                                                E-mail<br />
                                                No. Telpon / HP<br />
                                                Alamat<br />
                                            </IonCol>
                                            <IonCol size="1">
                                                :<br />
                                                :<br />
                                                :<br />
                                                :<br />
                                            </IonCol>
                                            <IonCol style={{ float: 'left' }}>
                                                {item.name}<br />
                                                {item.email}<br />
                                                {item.phone}<br />
                                                {item.address}<br />
                                            </IonCol>
                                        </IonRow>
                                        </IonCard>
                                    )
                                })}
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default User;