import {
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenu,
  IonTitle,
  IonToolbar,
  IonAlert,
  IonButton,
} from '@ionic/react';
import React, { useState } from 'react';
import { RouteComponentProps, withRouter, useHistory} from 'react-router-dom';
import { AppPage } from '../declarations';
import { arrowDropright, arrowDropdown, logOut } from 'ionicons/icons';
import {useAuth} from '../../authentication';

interface MenuProps extends RouteComponentProps {
  appPages: AppPage[];
}

const Menu: React.FC<MenuProps> = ({ appPages }, ) => {
  let history = useHistory();
  const [hide, setHide] = useState(true);
  const [hide2, setHide2] = useState(true);
  const [alert , showAlert] = useState(false);
  const [message, setMessage] = useState("");
  const auth = useAuth();

  function dropDown() {
    if (hide === true) {
      return <IonItem onClick={() => setHide(false)}>
        Katalog
      <IonIcon icon={arrowDropright} />
      </IonItem>;
    } else {
      return <IonItem onClick={() => setHide(true)}>
        Katalog
      <IonIcon icon={arrowDropdown}></IonIcon>
      </IonItem>;
    }
  }

  function dropDown2(){
    if (hide2 === true) {
      return <IonItem onClick={() => setHide2(false)}>
        Transaksi
      <IonIcon icon={arrowDropright} />
      </IonItem>;
    } else {
      return <IonItem onClick={() => setHide2(true)}>
        Transaksi
      <IonIcon icon={arrowDropdown}></IonIcon>
      </IonItem>;
    }
  }

  const logoutAdmin = () => {
    try{
      setMessage("Berhasil Keluar");
      showAlert(true);
    }catch(e){
      console.error(e);
    }
  }

  return (
    <IonMenu contentId="main" type="overlay">
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonTitle>Menu Admin</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>

        <IonAlert isOpen={alert} message={message} onDidDismiss={()=>showAlert(false)} backdropDismiss={false}
        buttons={[{text : 'OK', 
        handler: async () =>{
          await auth.signout();
          history.replace('/Login');
          history.go(0);
        }
        }
        ]} />

        <IonList>
          {appPages.map((appPage, index) => {
            return (
              index === 0 ?
                <IonItem routerLink={appPage.url} routerDirection="none" key={appPage.title}>
                  <IonIcon icon={appPage.icon}></IonIcon>
                  <IonLabel style={{paddingLeft:15}}>{appPage.title}</IonLabel>
                </IonItem> :

              index === 1 ?
                <IonItem key={appPage.title}>
                  <IonIcon icon={appPage.icon}></IonIcon>
                  {dropDown()}
                </IonItem> :
                
              index === 2 || index === 3?
                <IonItem style={{paddingLeft:30}} hidden={hide} routerLink={appPage.url} routerDirection="none" key={appPage.title}>
                  <IonIcon icon={appPage.icon}></IonIcon>
                  <IonLabel style={{marginLeft: 10}}>{appPage.title}</IonLabel>
                </IonItem> :

              index === 4 ?
                <IonItem key={appPage.title}>
                  <IonIcon icon={appPage.icon}></IonIcon>
                  {dropDown2()}
                </IonItem> :

                <IonItem style={{paddingLeft:30}} hidden={hide2} routerLink={appPage.url} routerDirection="none" key={appPage.title}>
                  <IonIcon icon={appPage.icon}></IonIcon>
                  <IonLabel style={{marginLeft: 10}}>{appPage.title}</IonLabel>
                </IonItem>
            );
          })}
          <IonItem color="danger" style={{paddingTop:30}}>
            <IonIcon icon={logOut}/>
            <IonButton onClick={logoutAdmin} color="light" style={{marginLeft: 10}}>
              <IonLabel>Logout</IonLabel>
            </IonButton>
          </IonItem>

        </IonList>
      </IonContent>
    </IonMenu>
  )
};

export default withRouter(Menu);
