import React, { useState, FormEvent } from 'react';
import {
    IonApp,
    IonCard,
    IonCardHeader,
    IonCardTitle,
    IonCardContent,
    IonItem,
    IonLabel,
    IonInput,
    IonButton,
    IonRow,
    IonCol,
    IonGrid,
    IonIcon,
    IonAlert,
    IonPopover,
    IonSpinner,
} from '@ionic/react';
import { eye, eyeOff } from 'ionicons/icons';
import './Login.css';
import { useAuth } from './authentication';
import { useHistory } from 'react-router';

const Login: React.FC = () => {
    const auth = useAuth();
    let history = useHistory();
    const [loading, setLoading] = useState(false);
    const [popOver, setPopOver] = useState(false);
    const [alert, showAlert] = useState(false);
    const [message, setMessage] = useState("");
    const [visible, setVisible] = useState(false);
    const [inputValue, setInputValue] = useState({
        email: '',
        password: '',
    })

    const handleVisibleOn = () => {
        setVisible(false);
    }

    const handleVisibleOff = () => {
        setVisible(true);
    }

    const login = async (e: FormEvent) => {
        e.preventDefault();
        setLoading(true);
        setPopOver(true);
        try {
            if (!inputValue.email) {
                setMessage("Login Gagal");
                showAlert(true);
            } else {
                await auth.signin(inputValue.email, inputValue.password);
                setMessage("Login Berhasil");
                showAlert(true);
            }
        } catch (err) {
            setMessage("Login Gagal");
            showAlert(true);
            console.error(err);
        }
        setPopOver(false);
        setLoading(false);
    }

    return (
        <IonApp>
            <IonAlert
                isOpen={alert} message={message} backdropDismiss={false} buttons={[{
                    text: 'OK',
                    handler: () => {
                        history.replace('/');
                    }
                }]} onDidDismiss={() => showAlert(false)} />
            <IonCard className="logCard">
                <IonCardHeader>
                    <IonCardTitle className="titleLog">Login Admin</IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                    {loading ?
                        <IonPopover isOpen={popOver}>
                            <IonLabel>Please Wait <IonSpinner name="circles" /></IonLabel>
                        </IonPopover> : ''
                    }
                    <IonGrid>
                        <IonRow>
                            <form onSubmit={login}>
                                <IonItem>
                                    <IonLabel position="floating" >Email</IonLabel>
                                    <IonInput
                                        placeholder="example@example.com"
                                        name="email"
                                        type="email"
                                        required
                                        value={inputValue.email}
                                        onIonChange={(e) => setInputValue({ ...inputValue, email: e.detail.value! })}
                                    />
                                </IonItem>
                                <IonItem>
                                    <IonCol size="15">
                                        <IonLabel position="floating" >Password</IonLabel>
                                        <IonInput
                                            name="password"
                                            type={visible ? "text" : "password"}
                                            required
                                            value={inputValue.password}
                                            onIonChange={(e) => setInputValue({ ...inputValue, password: e.detail.value! })}
                                        />
                                    </IonCol>
                                    <IonCol size="2" slot="end" className="visible">
                                        {visible ?
                                            <IonIcon icon={eye} onClick={handleVisibleOn} className="iconVisible" /> :
                                            <IonIcon icon={eyeOff} onClick={handleVisibleOff} className="iconVisible" />
                                        }
                                    </IonCol>
                                </IonItem>
                                <IonButton expand="block" shape="round" className="btn" type="submit"> Login </IonButton>
                            </form>
                        </IonRow>
                    </IonGrid>
                </IonCardContent>
            </IonCard>
        </IonApp>
    )
};

export default Login;