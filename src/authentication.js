import React, { useState, useEffect, useContext, createContext } from "react";
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
  apiKey: "AIzaSyDdF8rEkIhcCPWFGj0PNfU77F-A-5JjjZ0",
  authDomain: "ecom-8157d.firebaseapp.com",
  databaseURL: "https://ecom-8157d.firebaseio.com",
  projectId: "ecom-8157d",
  storageBucket: "ecom-8157d.appspot.com",
  messagingSenderId: "764540900022",
  appId: "1:764540900022:web:c59eb020012797f518cc17",
  measurementId: "G-MZGQTFN2ZG"
};

firebase.initializeApp(config);

const userContext = createContext();

export function ProvideAuth({ children }) {
  const auth = useProvideAuth();
  return <userContext.Provider value={auth}>{children}</userContext.Provider>;
}

export const useAuth = () => {
  return useContext(userContext);
};

function useProvideAuth() {
  const [state, setState] = useState(() => {
    const user = firebase.auth().currentUser;
    return { initializing: !user, user };
  });

  function onChange(user) {
    setState({ initializing: false, user });
  }

  const signin = async (email, password) => {
    const response = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password);
    setState({ initializing: false, user: response.user });
    return response.user;
  };

  const signout = async () => {
    await firebase
      .auth()
      .signOut();
    setState({ ...state, user: null });
  };

  const sendPasswordResetEmail = async email => {
    await firebase
      .auth()
      .sendPasswordResetEmail(email);
    return true;
  };

  const confirmPasswordReset = async (code, password) => {
    await firebase
      .auth()
      .confirmPasswordReset(code, password);
    return true;
  };

  useEffect(() => {
    const unsubscribe = firebase.auth().onAuthStateChanged(onChange);

    return () => unsubscribe();
  }, []);

  return {
    state,
    signin,
    signout,
    sendPasswordResetEmail,
    confirmPasswordReset,
  };
}